// Read file in html
const http=require('http')
let fs=require('fs')
http.createServer(function (req,res){
    fs.readFile('./demo.html',function(err,data){
        res.writeHead(200,{'content-type':'text/html'});
        res.write(data);

        res.end();
    });
}).listen(3000);

//append file

var fs=require('fs')
fs.appendFile('file.text','Hello content',function(err){
    if(err) throw err;
    console.log('saved')
})

//delete file

var fs=require('fs')
fs.unlink('file1.text',function(err){
    if(err) throw err;
    console.log('deleted')
})


// Rename filename

var fs=require('fs')
fs.rename('file.text','file2.text',function(err){
    if(err) throw err;
    console.log('file renamed')
})